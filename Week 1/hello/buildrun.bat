ECHO
ECHO -- This simple bat file is trying to [compile] the code.

ECHO -- Setting a variable to where I think csc.exe is.
SET csc=C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe

ECHO -- Trying to [compile] the .cs code into the .exe file.
%csc% /out:hello.exe hello_v2.cs

ECHO -- Trying to [run] the code if available. (Note - last working version will run!)
hello.exe
